<?php
namespace RoeBooks\Shop\Domain\Model;

use TYPO3\Flow\Annotations as Flow;
use RoeBooks\Shop\Domain\Model\Book;

/**
 * A row in the Basket
 */
class BasketItem {

    protected $book;

    /**
     * The amount
     * @var integer
     * @Flow\Validate(type="NumberRange", options={ "minimum"=1, "maximum"=20 })
     */
    protected $amount;

    /**
     * @return int
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * @param $amount
     */
    public function setAmount($amount) {
        $this->amount = $amount;
    }

    public function setBook($book) {
        $this->book = $book;
    }

    public function getBook() {
        return $this->book;
    }

    public function getPrice() {
        return $this->amount * $this->getBook()->getPrice();
    }

}
