<?php

namespace RoeBooks\Shop\Service;

use \TYPO3\Flow\Annotations\Inject;
use \TYPO3\Flow\Mvc\View\ViewInterface;
use \TYPO3\Flow\Security\Context;
use \RoeBooks\Shop\Domain\Model\SearchTerm;
use \RoeBooks\Shop\Domain\Model\Basket;

class CommonService {

    const CONTROLLER_BOOKS = "book";

    const CONTROLLER_CATEGORY = "category";

    const CONTROLLER_BASKET = "basket";

    const CONTROLLER_LOGIN = "login";

    /**
     * @Inject
     * @var \RoeBooks\Shop\Domain\Model\Basket
     */
    protected $basket;

    /**
     * @Inject
     * @var \RoeBooks\Shop\Domain\Model\SearchTerm
     */
    protected $searchTerm;

    /**
     * @Inject
     * @var \TYPO3\Flow\Security\Context
     */
    protected $securityContext;

    /**
     * @param string $searchTerm
     */
    public function changeSearchTerm($searchTerm) {
        $this->searchTerm->setSearchTerm($searchTerm);
    }

    /**
     * @param \TYPO3\Flow\Mvc\View\ViewInterface $viewInterface
     */
    public function prepareView(ViewInterface $view, $activeController) {
        $view->assign('basket', $this->basket);
        $view->assign('searchTerm', $this->searchTerm->getSearchTerm());
        $view->assign('account', $this->securityContext->getAccount());
        $view->assign('controller', array($activeController => true));
    }


}
