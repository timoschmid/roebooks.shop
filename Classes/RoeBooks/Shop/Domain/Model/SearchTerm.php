<?php

namespace RoeBooks\Shop\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "RoeBooks.Shop".              *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * A Search Term
 *
 * @Flow\Scope("session")
 */
class SearchTerm {
	
	/**
	 * The search term
	 * 
	 * @var string 
	 */
	protected $searchTerm;
	
	/**
	 * Returns the last search term
	 * 
	 * @return string
	 */
	public function getSearchTerm() {
		return $this->searchTerm;
	}
	
	/**
	 * Sets the last search Term
	 * 
	 * @param string $searchTerm
	 */
	public function setSearchTerm($searchTerm) {
		$this->searchTerm = $searchTerm;
	}
	
}