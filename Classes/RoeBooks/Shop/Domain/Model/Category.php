<?php
namespace RoeBooks\Shop\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "RoeBooks.Shop".              *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * A Category
 *
 * @Flow\Entity
 */
class Category {

	/**
	 * The name
	 * @var string
     * @Flow\Identity
	 */
	protected $name;

	/**
	 * The Books in this category
	 * @var \Doctrine\Common\Collections\ArrayCollection<\RoeBooks\Shop\Domain\Model\Book>
	 * @ORM\OneToMany(mappedBy="category")
	 */
	protected $books;


	/**
	 * Get the Category's name
	 *
	 * @return string The Category's name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets this Category's name
	 *
	 * @param string $name The Category's name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the categories books
	 */
	public function getBooks() {
		return $this->books;
	}

	/**
	 * Sets the books for this category
	 */
	public function setBooks($books) {
		$this->books = $books;
	}

}
?>
