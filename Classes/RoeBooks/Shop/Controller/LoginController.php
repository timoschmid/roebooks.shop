<?php

namespace RoeBooks\Shop\Controller;

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Security\Authentication\Controller\AbstractAuthenticationController;
use TYPO3\Flow\Security\AccountFactory;
use TYPO3\Flow\Error\Message as FlashMessage;
use RoeBooks\Shop\Service\CommonService;

/**
 * The Controller for Login, Logout, Registration
 */
class LoginController extends AbstractAuthenticationController {

    /**
     * @var \TYPO3\Flow\Security\AccountFactory
     * @Flow\Inject
     */
    protected $accountFactory;

    /**
     * @var \RoeBooks\Shop\Service\CommonService
     * @Flow\Inject
     */
    protected $commonService;


    public function signupAction() {
        // show template
        $this->commonService->prepareView($this->view, CommonService::CONTROLLER_LOGIN);
    }

    public function loginAction() {
        $this->commonService->prepareView($this->view, CommonService::CONTROLLER_LOGIN);
    }

    /**
     * Called when a new user is registered
     * @param string $username The username
     * @param string $password
     * @param string $passwordConfirm
     * @return void
     */
    public function registerAction($username, $password, $passwordConfirm) {
        if($password == $passwordConfirm) {
            $account = $this->accountFactory->createAccountWithPassword($username, $password, array('customer'));
            $this->persistenceManager->add($account);
            $this->flashMessageContainer->addMessage(new FlashMessage("An account was created for you."));
            $this->redirect("index", "Book");
        } else {
            throw new Exception("Your passwords don't match.");
        }
    }

    /**
     * Called when the user is authenticated successfully
     * @param \TYPO3\Flow\Mvc\ActionRequest $originalRequest
     * @return void
     */
    public function onAuthenticationSuccess(\TYPO3\Flow\Mvc\ActionRequest $originalRequest = null) {
        $this->flashMessageContainer->addMessage(new FlashMessage("Logged in successful."));
        $this->redirect("index", "Book");
    }

    /**
     * The logout action
     */
    public function logoutAction() {
        parent::logoutAction();
        $this->redirect("index", "Book");
    }

}

?>
