<?php
namespace RoeBooks\Shop\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "RoeBooks.Shop".              *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;

use \TYPO3\Flow\Mvc\Controller\ActionController;
use \RoeBooks\Shop\Domain\Model\Book;
use \RoeBooks\Shop\Domain\Model\Basket;
use \RoeBooks\Shop\Service\CommonService;

/**
 * Book controller for the RoeBooks.Shop package
 *
 * @Flow\Scope("singleton")
 */
class BookController extends ActionController {

	/**
	 * @Flow\Inject
	 * @var \RoeBooks\Shop\Domain\Repository\BookRepository
	 */
	protected $bookRepository;

	/**
	 * @Flow\Inject
	 * @var \RoeBooks\Shop\Domain\Repository\CategoryRepository
	 */
	protected $categoryRepository;

    /**
     * @var \RoeBooks\Shop\Service\CommonService
     * @Flow\Inject
     */
    protected $commonService;

	/**
	 * Shows a list of books
	 *
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('books', $this->bookRepository->findAll());
        $this->commonService->prepareView($this->view, CommonService::CONTROLLER_BOOKS);
	}

	/**
	 * Search for books
	 *
	 * @param string $q The search term
	 * @return void
	 */
	public function searchAction($q) {
		$this->commonService->changeSearchTerm($q);
		$this->view->assign('books', $this->bookRepository->findBySearchTerm($q));
        $this->commonService->prepareView($this->view, CommonService::CONTROLLER_BOOKS);
	}

	/**
	 * Shows a single book object
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $book The book to show
	 * @return void
	 */
	public function showAction(Book $book) {
		$this->view->assign('book', $book);
        $this->commonService->prepareView($this->view, CommonService::CONTROLLER_BOOKS);
	}

	/**
	 * Shows a form for creating a new book object
	 *
	 * @return void
	 */
	public function newAction() {
		$this->view->assign('categories', $this->categoryRepository->findAll());
        $this->commonService->prepareView($this->view, CommonService::CONTROLLER_BOOKS);
	}

	/**
	 * Adds the given new book object to the book repository
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $newBook A new book to add
	 * @return void
	 */
	public function createAction(Book $newBook) {
		$this->bookRepository->add($newBook);
		$this->addFlashMessage('Created a new book.');
		$this->redirect('index');
	}

	/**
	 * Shows a form for editing an existing book object
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $book The book to edit
	 * @return void
	 */
	public function editAction(Book $book) {
		$this->view->assign('book', $book);
		$this->view->assign('categories', $this->categoryRepository->findAll());
		$this->commonService->prepareView($this->view, CommonService::CONTROLLER_BOOKS);
	}

	public function initializeUpdateAction() {
		// $this->arguments['book']->getPropertyMappingConfiguration()->forProperty('image')->allowProperties('__identity');
	}

	/**
	 * Updates the given book object
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $book The book to update
	 * @return void
	 */
	public function updateAction(Book $book) {
		$this->bookRepository->update($book);
		$this->addFlashMessage('Updated the book.');
		$this->redirect('index');
	}

	/**
	 * Removes the given book object from the book repository
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $book The book to delete
	 * @return void
	 */
	public function deleteAction(Book $book) {
		$this->bookRepository->remove($book);
		$this->addFlashMessage('Deleted a book.');
		$this->redirect('index');
	}

}

?>
