<?php
namespace RoeBooks\Shop\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "RoeBooks.Shop".              *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * A Basket
 *
 * @Flow\Scope("session")
 */
class Basket {

	/**
	 * The books
	 * @var \Doctrine\Common\Collections\Collection<\RoeBooks\Shop\Domain\Model\BasketItem>
	 * @ORM\ManyToMany
	 */
	protected $items;

	/**
	 *
	 */
	public function __construct() {
		$this->items = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get the Basket's books
	 *
	 * @return \Doctrine\Common\Collections\Collection<\RoeBooks\Shop\Domain\Model\BasketItem> The Basket's books
	 */
	public function getItems() {
		return $this->items;
	}

	/**
	 * Adds a book to the basket
	 *
	 * @param \RoeBooks\Shop\Domain\Model\BasketItem $book The book to add
	 * @return void
	 * @Flow\Session(autoStart=true)
	 */
	public function addItem(BasketItem $newItem) {
        foreach($this->items as $item) {
            if($item->getBook() == $newItem->getBook()) {
                $item->setAmount($item->getAmount() + $newItem->getAmount());
                return;
            }
        }
		$this->items->add($newItem);
	}

	/**
	 * Adds a book to the basket
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $book The book to remove
	 * @return void
	 */
	public function removeItem(Book $book) {
        foreach($this->items as $item) {
            if($item->getBook() == $book) {
                return $this->items->removeElement($item);
            }
        }
	}

    /**
     * Returns the total amount of articles in the basket
     * @return int The total amount of articles in the basket
     */
    public function getAmount() {
        $amount = 0;
        foreach($this->items as $item) {
            $amount += $item->getAmount();
        }
        return $amount;
    }

    /**
     * Returns the total price of the basket
     * @return int The total price of the basket
     */
    public function getTotal() {
        $total = 0;
        foreach($this->items as $book) {
            $total += $book->getPrice();
        }
        return $total;
    }

}
?>
