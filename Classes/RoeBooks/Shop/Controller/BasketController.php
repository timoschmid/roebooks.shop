<?php
namespace RoeBooks\Shop\Controller;

use TYPO3\Flow\Annotations\Inject;
use TYPO3\Flow\Annotations\Scope;
use TYPO3\Flow\Mvc\Controller\ActionController;
use RoeBooks\Shop\Domain\Model\Book;
use RoeBooks\Shop\Domain\Model\Basket;
use RoeBooks\Shop\Domain\Model\BasketItem;
use RoeBooks\Shop\Service\CommonService;

/**
 * Basket controller for the RoeBooks.Shop package
 *
 * @Scope("singleton")
 */
class BasketController extends ActionController {

    /**
     * @Inject
     * @var \RoeBooks\Shop\Domain\Model\Basket
     */
    protected $basket;

    /**
     * @var \RoeBooks\Shop\Service\CommonService
     * @Inject
     */
    protected $commonService;

	/**
	 *
	 */
	public function indexAction() {
        $this->commonService->prepareView($this->view, CommonService::CONTROLLER_BASKET);
	}

	/**
	 * Adds a book to the shopping basket
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $book
     * @param int $amount
	 * @return void
	 */
	public function addAction(Book $book, $amount = 1) {
        $basketItem = new BasketItem();
        $basketItem->setAmount($amount);
        $basketItem->setBook($book);
        $this->basket->addItem($basketItem);
		$this->addFlashMessage('Added "' . $book->getTitle() . '" to your shopping basket.');
		$this->redirect('index', 'Book');
	}

    /**
     * Increases the amount of a Book by 1
     * @param \RoeBooks\Shop\Domain\Model\Book $book
     * @return void
     */
    public function increaseAction(Book $book) {
        $this->persistenceManager;
        foreach($this->basket->getItems() as $item) {
            if($item->getBook() == $book) {
                $item->setAmount($item->getAmount() + 1);
            }
        }
        $this->addFlashMessage('Added one "' . $book->getTitle() . '" to your shopping basket.');
        $this->redirect('index', 'Basket');
    }

    /**
     * Removes exactly 1 Book from the basket
     * @param \RoeBooks\Shop\Domain\Model\Book $book
     * @return void
     */
    public function decreaseAction(Book $book) {
        foreach($this->basket->getItems() as $item) {
            if($item->getBook() == $book) {
                if($item->getAmount() > 1) {
                    $item->setAmount($item->getAmount() - 1);
                    $this->addFlashMessage('Removed one "' . $book->getTitle() . '" from your shopping basket.');
                } else {
                    $this->basket->removeItem($book);
                    $this->addFlashMessage('Removed "' . $book->getTitle() . '" from your shopping basket.');
                }
            }
        }
        $this->redirect('index', 'Basket');
    }

	/**
	 * Removes all from a Book from the shopping basket
	 *
	 * @param \RoeBooks\Shop\Domain\Model\Book $book
	 * @return void
	 */
	public function removeAction(Book $book) {
		$this->basket->removeItem($book);
		$this->addFlashMessage('Removed "' . $book->getTitle() . '" from your shopping basket.');
		$this->redirect('index');
	}

    /**
     * @param \RoeBooks\Shop\Domain\Model\Basket $basket
     */
    public function injectBasket(Basket $basket) {
        $this->basket = $basket;
    }

    /**
     * @param \RoeBooks\Shop\Service\CommonService $commonService
     * @return void
     */
    public function injectCommonService(\RoeBooks\Shop\Service\CommonService $commonService) {
        $this->commonService = $commonService;
    }

}

?>
