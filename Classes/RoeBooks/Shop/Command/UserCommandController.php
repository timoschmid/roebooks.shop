<?php

namespace RoeBooks\Shop\Command;

use \TYPO3\Flow\Annotations as Flow;
use \TYPO3\Flow\Cli\CommandController;

/**
 * @Flow\Scope("singleton")
 */
class UserCommandController extends CommandController {

    /**
     * @var \TYPO3\Flow\Security\AccountRepository
     * @Flow\Inject
     */
    protected $accountRepository;

    /**
     * @var \TYPO3\Flow\Security\AccountFactory
     * @Flow\Inject
     */
    protected $accountFactory;

    /**
     * @var \TYPO3\Flow\Persistence\PersistenceManagerInterface
     * @Flow\Inject
     */
    protected $persistenceManager;

    /**
     * Create a admin user.
     *
     * Create a admin user with the given username, password and roles.
     *
     * @param string $username aaaa
     * @param string $password aaa
     * @return string
     */
    public function createAdminCommand($username, $password) {
        $this->createUser($username, $password, 'admin');
    }

    /**
     * Create a editor user.
     *
     * Create a editor user with the given username, password and roles.
     *
     * @param string $username aaaa
     * @param string $password aaa
     * @return string
     */
    public function createEditorCommand($username, $password) {
        $this->createUser($username, $password, 'editor');
    }

    private function createUser($username, $password, $role) {
        $existingUser = $this->accountRepository->findOneByAccountIdentifier($username);
        if($existingUser) {
            $this->outputLine("Cannot create the %s '%s': User already exists.", array($role, $username));
            $this->quit(-1);
        } else {
            $account = $this->accountFactory->createAccountWithPassword($username, $password, array($role));
            $this->persistenceManager->add($account);
            $this->outputLine("Created the %s '%s'.", array($role, $username));
            $this->quit();
        }
    }

}

?>
