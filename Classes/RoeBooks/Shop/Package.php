<?php
namespace RoeBooks\Shop;

use TYPO3\Flow\Package\Package as BasePackage;
use TYPO3\Flow\Annotations as Flow;

/**
 * Package base class of the RoeBooks.Shop package.
 *
 * @Flow\Scope("singleton")
 */
class Package extends BasePackage {

}
?>