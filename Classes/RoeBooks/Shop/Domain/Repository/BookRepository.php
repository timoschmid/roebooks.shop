<?php
namespace RoeBooks\Shop\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "RoeBooks.Shop".              *
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Persistence\QueryInterface;

use TYPO3\Flow\Annotations as Flow;

/**
 * A repository for Books
 *
 * @Flow\Scope("singleton")
 */
class BookRepository extends \TYPO3\Flow\Persistence\Repository {

	/**
	 * Search for articles
	 * @param string $q
	 */
	public function findBySearchTerm($q) {
		$query = $this->createQuery();
		return $query->matching(
				$query->logicalOr(
					$query->like('title', '%'.$q.'%'),
					$query->like('description', '%'.$q.'%')
				)
		)->setOrderings(array('title' => QueryInterface::ORDER_ASCENDING))->execute();
	}

}
?>